package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumberrsBagOperations;

public class Controller {

	private static NumberrsBagOperations model = new NumberrsBagOperations();
	
	
	public static NumbersBag createBag(ArrayList<Integer> values){
         return new NumbersBag(values);		
	}
	public static double getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	public static double getMin(NumbersBag bag){
		return model.getMin(bag);
	}
	public static double getAverage(NumbersBag bag){
		return model.getAverage(bag);
	}
	public static double contarNumerosMayoresA(NumbersBag bag, int a){
		return model.contarNumerosMayoresA(bag, a);
		
	}
}
