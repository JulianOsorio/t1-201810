package model.logic;

import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumberrsBagOperations {
	
	public double computeMean(NumbersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(NumbersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	public int getAverage(NumbersBag bag){
		int average=0;
		int total=0;
		int suma=0;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				total++;
				suma+=iter.next();
			}
		}
		return average= suma/total;
	}
	
	public int getMin(NumbersBag bag){
		int min = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
			
		}
		return min;
	}
	public int contarNumerosMayoresA(NumbersBag bag, int a){
		int cont=0;
		if( bag!=null ){
			Iterator<Integer> iter=bag.getIterator();
			while(iter.hasNext()){
				if(iter.next()>a){
					cont++;
				}
			}
		}
		return cont;
	}
}
